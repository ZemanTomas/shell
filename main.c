#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <string.h>
#include <errno.h>   
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/types.h> 
#include <readline/readline.h>
#include <readline/history.h>

#define LINE_MAX 65335
#define CWD_MAX 65335

void handler(){
    rl_reset_line_state();
    rl_cleanup_after_signal();
    rl_replace_line("",0);
    rl_crlf();
    rl_redisplay();
    signal(SIGINT,handler);
}

void readline_wrap(char *line);

typedef struct cmd cmd;
struct cmd{
  int argc;
  char *cmd;
  char **argv;
  cmd *next;
  //PIPES
};
cmd *tokenize(char *);
char **add(char **,char *,int);
void freecmd(cmd *);
int execute(cmd *,int);

//Commands
//int internal_cd(char*);
//int internal_exit(); <--- is implicit


typedef struct options options;
struct options{
  bool cmode;
  bool imode;
  bool ver;
  char *cmodecmd;
  char *imodecmd;
};
options *getoptions(int , char **);

int parseline(char *);

char *getprompt();
char *prompt = NULL;
int global_line = 0; //Should be 1?
int global_ret = 0;
int global_exited = 0;
options *opt;
int main(int argc, char **argv){
  signal(SIGINT,handler);
  opt = getoptions(argc,argv);
  prompt = getprompt();
  if(opt->ver){
    printf("mysh V0.2\n");
    return 0;
  }
  if(opt->cmode && opt->cmodecmd != NULL){
    global_ret = parseline(opt->cmodecmd);
  }else{
    if(opt->imode){
      rl_callback_handler_install(prompt, (rl_vcpfunc_t*) &readline_wrap);
      while(global_exited == 0){
        rl_callback_read_char();
      }
    }else{
      int file = open(opt->imodecmd,O_RDONLY),nbytes = 0;
      char buf[LINE_MAX];
      while((nbytes = read(file, buf,LINE_MAX)) > 0 || global_exited == 0){
        char *tmp = malloc(nbytes*sizeof(char));
        memcpy(tmp,buf,nbytes);
        tmp[nbytes]='\0';
        global_ret = parseline(tmp);
        free(tmp);
      }
      close(file);
    }
  }
  rl_callback_handler_remove();
  free(opt);
  free(prompt);
  return global_ret;
}

void readline_wrap(char *line){
  if(line==NULL){//^D
    printf("exit\n");
    global_exited = 1;
    exit(global_ret);
  }else{
    if(*line!=0){
      add_history(line);
      global_ret = parseline(line);
    }
    free(line);
  }
}

options *getoptions(int argc, char **argv){
  int index,c;
  opterr = 0;
  options *opt = malloc(sizeof(options));
  opt->cmode = false;
  opt->imode = true;
  opt->ver = false;
  opt->cmodecmd = NULL;
  opt->imodecmd = NULL;

  while((c = getopt (argc, argv, "vc:")) != -1){
    switch (c){
      case 'v':
        opt->ver = true;
        break;
      case 'c':
        opt->cmode = true;
        opt->cmodecmd = optarg;
        break;
      case '?':
        if(optopt == 'c'){
          write(2, "Option -%c requires an argument.\n", sizeof("Option -%c requires an argument.\n"));
          opt->cmode = true;
        }
        break;
      default:
        write(2,"Default\n",8);
        abort();
      }
  }

  for (index = optind; index < argc; index++){
    opt->imode = false;
    opt->imodecmd = argv[index];
  }
  return opt;
}

cmd *tokenize(char *line){
  int j = 0,linec = strlen(line);
  cmd* ret = malloc(sizeof(cmd));
  cmd* cur = ret;
  cur->cmd = NULL;
  cur->next = NULL;
  cur->argc = 0;

  int i = 0;
  for(;i < linec && line[i] != '#';){
    if(i>0){
      if(cur->cmd == NULL){
        freecmd(ret);
        printf("error:%d: syntax error near unexpected token ';'\n",global_line);
        return NULL;
      }
      cmd* next = malloc(sizeof(cmd));
      cur->next = next;
      cur = next;
      cur->cmd = NULL;
      cur->next = NULL;
      cur->argc = 0;
    }
    bool inword = false;
    for(;i < linec && line[i] != '#' && line[i] != ';';i++){
      if(isspace(line[i])){
        if(inword){
          char *tmp = malloc((i-j+1)*sizeof(char));
          memcpy(tmp,&line[j],i-j);
          tmp[i-j]='\0';
          if(cur->cmd == NULL){
              cur->cmd = malloc((i-j+1)*sizeof(char));
              strcpy(cur->cmd,tmp);
          }else{
            //
          }
          cur->argv = add(cur->argv,tmp,cur->argc++);
          inword = false;
        }else{
          //
        }
      }else{
          if(inword){
            //
          }else{
            inword = true;
            j = i;
          }
      }
    }
    if(inword){
      char *tmp = malloc((i-j+1)*sizeof(char));
      memcpy(tmp,&line[j],i-j);
      tmp[i-j]='\0';
      if(cur->cmd == NULL){
        cur->cmd = malloc((i-j+1)*sizeof(char));
        strcpy(cur->cmd,tmp);
      }else{
        //
      }
      cur->argv = add(cur->argv,tmp,cur->argc++);
    }
    cur->argv = add(cur->argv,NULL,cur->argc);
  }
  return ret;
}

char **add(char **arr,char *str,int oldsize){
  char **tmp = malloc((oldsize+1)*sizeof(char *));
  for(int i = 0; i<oldsize; i++){
    tmp[i]=arr[i];
  }
  tmp[oldsize] = str;
  free(arr);
  return tmp;
}

void freecmd(cmd *c){
  if(c == NULL){
    return;
  }
  free(c->cmd);
  if(c->next != NULL){
    freecmd(c->next);
  }
  for(int i = 0; i< c->argc; i++){
    free(c->argv[i]);
  }
  free(c->argv);
  free(c);
}

int parseline(char *line){
  int ret = global_ret;
  global_line++;
  cmd *command = tokenize(line);
  cmd *first = command;
  if(command != NULL && command->cmd != NULL){
    while(command != NULL){
      if(strcmp(command->cmd,"exit") == 0){
        global_exited = 1;
        break;
      }else{
      ret = execute(command,ret);
      }
      command = command->next;
    }
  }else{
    ret = 1;
  }
  freecmd(first);
  return ret;
}
int execute(cmd *command,int ret){
  int stat;
  pid_t pid = fork();
  if(pid == -1){/*ERROR, set ret*/
  }else if(pid == 0){
    ret = execvp(command->cmd,command->argv);
    exit(0);
  }else{
    if(waitpid(pid,&stat,0) > 0){
      if(WIFEXITED(stat) && !WEXITSTATUS(stat)){
        /*success*/
      }else if(WIFEXITED(stat) && WEXITSTATUS(stat)){
        if(WEXITSTATUS(stat) == 127){
          /*exec failed*/
        //printf("Ex.failed,%d\n",WEXITSTATUS(stat));
        }else{
          /*Program returned non zero status*/
        //printf("Nonzero,%d\n",WEXITSTATUS(stat));
        }
      }else{
        /*not normal*/
        //printf("notnormal,%d\n",WEXITSTATUS(stat));
      }
    }//else waitpid fail
        //printf("wfail,%d\n",WEXITSTATUS(stat));
    ret = WEXITSTATUS(stat);
  }
  return ret;
}

//int internal_cd(char * arg){

//}

char *getprompt(){
  free(prompt);
  char *prefix = "mysh:";
  char cwd[CWD_MAX];
  if(getcwd(cwd,CWD_MAX) == NULL){
    cwd[0] = '\0';
  }
  char *suffix = "$ ";
  char *res = malloc(((int)strlen(prefix)+(int)strlen(suffix)+(int)strlen(cwd))*sizeof(char));
  strcat(res,prefix);
  strcat(res,cwd);
  strcat(res,suffix);
  return res;
}
